import React, { LC, MultiGather, Yeet, useFiber, useContext, useCapture, makeContext, makeCapture } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
  tab: 'props',
};

export const GatherExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={4}
      save={false}
    >
      <Component />
    </UseInspect>
  );
};

const Component = () => (<>
  <MultiGather
    children={<>
      <Child />
      <Group>
        <Child />
        <Group>
          <Child />
          <Child />
        </Group>
        <Child />
        <Group>
          <Group>
            <Child />
            <Child />
          </Group>            
          <Child />
        </Group>
      </Group>
      <Child />
    </>}
    then={(record: Record<string, number[]>) => {
    }}
  />
</>);

const random = () => Math.floor(Math.random() * 100);

const Group = (props) => props.children;
const Child = (props) => <Yeet>{
    random() > 66 ? {hello: random()} :
    random() > 33 ? {world: random()} :
    {hello: random(), world: random()}
  }</Yeet>;

// Bundler insists on renaming these
Group.displayName = 'Group';
Child.displayName = 'Child';
GatherExample.displayName = 'GatherExample';
Component.displayName = 'Component';
