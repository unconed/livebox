import React, { LC, Capture, Gather, Yeet, useFiber, makeCapture } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  fullSize: isMobile(),
  builtins: true,
  tab: 'props',
};

export const InliningExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={4}
      save={false}
    >
      <WithInlining />
      <Space />
      <WithoutInlining />
    </UseInspect>
  );
};

const WithInlining = (props) => (
  <Gather
    children={
      <Component1 />
    }
  />
);
WithInlining.displayName = 'Inlined';

const WithoutInlining = (props) => (
  [<Gather
    children={
      <Component2 />
    }
  />]
);

WithoutInlining.displayName = 'Not Inlined';

const Component1 = (props) => (
  <Gather
    children={[
      <Group>
        <Child1 />
      </Group>,
      <Child1 />
    ]}
    then={(x) => <Yeet>{x}</Yeet>}
  />
);

const Component2 = (props) => (
  [<Gather
    children={[
      <Group>
        <Child2 />
      </Group>,
      <Child2 />
    ]}
    then={(x) => <Yeet>{x}</Yeet>}
  />]
);

const Child1 = (props) => <Yeet>{31337}</Yeet>;
const Child2 = (props) => [<Yeet>{31337}</Yeet>];

const Group = (props) => props.children;
const Space = (props) => { /* Invisible fiber */ null; }

Space.displayName = '       ';
Component1.displayName = 'Component';
Component2.displayName = 'Component';
Group.displayName = 'Group';
Child1.displayName = 'Component';
Child2.displayName = 'Component';
