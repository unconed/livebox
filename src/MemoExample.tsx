import React, { LC, Gather, Fence, Yeet, useFiber, useMemo, useState, useResource, memo } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
  tab: 'fiber',
};

export const MemoExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={8}
      save={false}
    >
      <Component />
    </UseInspect>
  );
};


const Component = (props) => {
  const [value] = useMemo(
    () => 'Memoized value',
    ['dependency 1', 'dependency 2']
  );

  const [state, setState] = useState(0);
  useResource((dispose) => {
    const timer = setInterval(
      () => setState(s => s + 1),
      1000
    );
    dispose(() => clearInterval(timer));
    return timer;
  }, []);
  
  return (<>
    <Group>
      <Child />
      <Group>
        <Child />
        <Child />
      </Group>
    </Group>
    <Child />
    <Group>
      <Child />
      <Child />
    </Group>
  </>);
}

const Group = props => props.children;
const Child = memo(props => props.children, 'Child');

Component.displayName = 'Component';
Group.displayName = 'Group';
