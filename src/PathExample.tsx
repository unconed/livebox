import React, { LC, Gather, Fence, Yeet, useFiber } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
  tab: 'fiber',
};

export const PathExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={7}
      save={false}
    >
      <Static />
      <Static>
        <Keyed key={'A'} />
        <Keyed key={'B'}>
          <Static />
          <Static />
        </Keyed>
        <Keyed key={'C'}>
          <Static />
          <Static />
        </Keyed>
      </Static>
      <Continued before={[<Static />, <Static />]} after={[<Static />, <Static />]} />
      <Static>
        <OnlyChild>
          <OnlyChild>
            <OnlyChild>
              <Static />
              <Static />
            </OnlyChild>
          </OnlyChild>
        </OnlyChild>
      </Static>
    </UseInspect>
  );
};

const Static = (props) => props.children;
const Keyed = (props) => props.children;
const OnlyChild = (props) => props.children;
const ArrayOfLength1 = (props) => props.children;

const Continued = (props) => <Fence children={props.before} then={() => props.after} />;
