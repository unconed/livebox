import React, { LC, Provide, Capture, useFiber, useContext, useCapture, makeContext, makeCapture } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
  tab: 'fiber',
};

export const ContextExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={4}
      save={false}
    >
      <Component />
    </UseInspect>
  );
};

const ExampleContext = makeContext<string>('Empty', 'ExampleContext');
const ExampleCapture = makeCapture<number>('ExampleCapture');

const Component = () => (<>
  <Provide
    context={ExampleContext}
    value={"Hello World"}
  >
    <Group>
      <UsesContext />
    </Group>
    <Group>
      <Child />
      <UsesContext>
        <Child />
      </UsesContext>
    </Group>
  </Provide>

  <Capture
    context={ExampleCapture}
    children={<>
      <Group>
        <UsesCapture />
      </Group>
      <Group>
        <Child />
        <UsesCapture>
          <Child />
        </UsesCapture>
      </Group>
    </>}
    then={() => <Child />}
  />
</>);

const random = () => Math.floor(Math.random() * 100);

const Group = (props) => props.children;
const Child = (props) => props.children;

const UsesContext = (props) => {
  const context = useContext(ExampleContext);
  return props.children;
};

const UsesCapture = (props) => {
  useCapture(ExampleCapture, random());
  return props.children;
};

// Bundler insists on renaming these
Group.displayName = 'Group';
Child.displayName = 'Child';
