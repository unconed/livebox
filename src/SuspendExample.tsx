import React, { LC, Gather, Yeet, memo, useFiber, useAwait, useResource, useState, SUSPEND } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { Suspense } from '@use-gpu/workbench';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
  tab: 'props',
};

export const SuspendExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      findFiber={6}
      save={false}
    >
      <Component />
    </UseInspect>
  );
};

const renderAsyncLevel = (render) => (version) => (
  <Async version={version} render={render} />
);

const renderAsyncTree = renderAsyncLevel(renderAsyncLevel(renderAsyncLevel((version) => <Child version={version} />)));

const Component = () => {
  
  const [version, setVersion] = useState(0);
  useResource((dispose) => {
    const timer = setInterval(() => setVersion(s => s + 1), 3000);
    dispose(() => clearInterval(timer));
  });

  return (
    <Gather
      children={<>
        <Child />
        <Suspense
          children={
            <Async version={version} render={renderAsyncTree} />
          }
          fallback={() => yeet(["Loading..."])}
        />
        <Child />
      </>}
      then={(record: Record<string, number[]>) => <Result result={record} />}
    />
  );
};

const random = () => Math.floor(Math.random() * 100);

const Async = memo((props) => {
  const [pending, setPending] = useState(false);

  const {version} = props;
  const [value, error] = useAwait(() => {
    setPending(true);
    return new Promise(resolve => setTimeout(() => {
      resolve(version + 1);
      setPending(false);
    }, 500));
  }, [version]);

  return [
    <Yeet>{pending ? SUSPEND : []}</Yeet>,
    value && props.render(value),
  ];
}, 'Async');

const Child = (props) => <Yeet>{`${random()}`}</Yeet>;

const Result = (props) => null;

// Bundler insists on renaming these
Child.displayName = 'Child';
SuspendExample.displayName = 'SuspendExample';
Component.displayName = 'Component';
