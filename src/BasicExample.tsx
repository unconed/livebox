import React, { LC, Gather, Yeet, useFiber } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
  select: false,
  skip: 3,
};

const INSPECT_STATE = {
  open: true,
  splitBottom: 50,
  fullSize: true,
  builtins: true,
};

export const BasicExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      save={false}
    >
      <>
        <>
          <Hello />
          <This />
        </>
      </>
    </UseInspect>
  );
};

const Hello = () => {
  return <World />;
};

const World = () => {};

const This = () => {
  return <Gather children={[
    <Is />,
    <Entirely />,
  ]} then={(values: number[]) => <Live values={values} />} />
};

const Live = () => {};

const Is = () => {
  return <Yeet>{31337}</Yeet>;
};

const Entirely = () => {
  return <Yeet>{31337}</Yeet>;
};

