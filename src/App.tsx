import React, { LC, hot, useFiber, useResource, render, unmount } from '@use-gpu/live';
import { Router, Routes } from '@use-gpu/workbench';
import '@use-gpu/inspect/theme.css';

import { BasicExample } from './BasicExample';
import { HookExample } from './HookExample';
import { PathExample } from './PathExample';
import { MemoExample } from './MemoExample';
import { InliningExample } from './InliningExample';
import { ContextExample } from './ContextExample';
import { GatherExample } from './GatherExample';
import { SuspendExample } from './SuspendExample';
import { ReconcileExample } from './ReconcileExample';
import { QueueExample } from './QueueExample';

const Sandbox = (props) => {
  const {children} = props;
  useResource((dispose) => {
    const fiber = render(children);
    dispose(() => unmount(fiber));
  });
};

const sandbox = el => <Sandbox>{el}</Sandbox>;

const ROUTES = {
  '/basic':      {element: sandbox(<BasicExample />)},
  '/hook':       {element: sandbox(<HookExample />)},
  '/path':       {element: sandbox(<PathExample />)},
  '/memo':       {element: sandbox(<MemoExample />)},
  '/inlining':   {element: sandbox(<InliningExample />)},
  '/context':    {element: sandbox(<ContextExample />)},
  '/gather':     {element: sandbox(<GatherExample />)},
  '/suspend':    {element: sandbox(<SuspendExample />)},
  '/reconcile':  {element: sandbox(<ReconcileExample />)},
  '/queue':      {element: sandbox(<QueueExample />)},
};

export const App: LC = hot(() => {
  const fiber = useFiber();

  const root = document.querySelector('#use-gpu')!;
  const inner = document.querySelector('#use-gpu .canvas')!;

  return (
    <Router hash><Routes routes={ROUTES} /></Router>
  );
}, module);

App.displayName = 'App';
