export const isMobile = () => window.innerWidth < 768;
export const isNarrow = () => window.innerWidth < 500;