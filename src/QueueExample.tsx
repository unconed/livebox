import React, { LC, Signal, Provide, Fence, Gather, Reconcile, Quote, Unquote, Yeet, useFiber, useMemo, useState, makeContext } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isNarrow } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: true,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 60,
  splitBottom: 50,
  fullSize: isNarrow(),
  builtins: true,
};

type PresentContextProps = { step: number, length: number };
const PresentContext = makeContext<PresentContextProps>({ step: 0, length: 0 }, 'PresentContext');

export const QueueExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      save={false}
      findFiber={15}
    >
      <Device>
        
        <View>
          <Present>
            <Slide>
              <Object />
              <Object />
            </Slide>
            <Slide>
              <Object />
              <Object />
            </Slide>
          </Present>
        </View>

      </Device>
    </UseInspect>
  );
};

const Device = (props) => {
  const device = {device: new Proxy({}, {})};
  return <Queue device={device}>{props.children}</Queue>
};

const Queue = (props) => {
  return (
    <Reconcile>
      <Quote>
        <Gather
          children={<Unquote>{props.children}</Unquote>}
          then={(things: any) => { return <Render things={things} /> }}
        />
      </Quote>
    </Reconcile>
  );
};

const View = (props) => {
  return (
    <Gather
      children={props.children}
      then={(objects: any[]) => {
        // Quote-yeet a lambda into the queue to draw objects every frame
        return (
          <Quote><Yeet>{() => {
            // ...
          }}</Yeet></Quote>
        );
      }}
    />
  );
}

const Present = (props) => {
  const [step, setStep] = useState(0);
  const [length, setLength] = useState(0);
  const context = useMemo(() => ({step, length}), [step, length]);

  return (
    <Reconcile>
      <Gather
        children={
          // Render all children as a quote, and unquote in `Slide` to build the `steps` reduction
          <Quote>
            <Signal />
            <Provide context={PresentContext} value={context}>
              {props.children}
            </Provide>
          </Quote>
        }
        then={(steps: any[]) => {
          setLength(steps.length);
        }}
      />
    </Reconcile>
  )
};

const Slide = (props) => {
  const {id} = useFiber();
  return (
    <Unquote>
      {/* Top tree collects slide IDs via gather */}
      <Yeet>{{id}}</Yeet>
      <Quote>
        {/* Bottom tree collects objects from layout, grouped by slide ID */}
        <Fence
          children={<Layout>{props.children}</Layout>}
          then={(objects: any[]) =>
            <Yeet>{{
              id,
              objects,
            }}</Yeet>}
        />
      </Quote>
    </Unquote>
  )
};

const Object = (props) => <Yeet>{{ point: [Math.random(), Math.random(), Math.random()] }}</Yeet>;

const Layout = (props) => props.children;

const Render = (props) => props.children;

// Bundler insists on renaming these
Layout.displayName = 'Layout';
Queue.displayName = 'Queue';
Render.displayName = 'Render';
Slide.displayName = 'Slide';
Object.displayName = 'Object';
Present.displayName = 'Present';
