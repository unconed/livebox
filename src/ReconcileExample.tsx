import React, { LC, Reconcile, Quote, Unquote, useFiber } from '@use-gpu/live';

import { UseInspect } from '@use-gpu/inspect';
import { isMobile } from './util';

const APPEARANCE = {
  toolbar: false,
  close: false,
  legend: false,
  resize: false,
};

const INSPECT_STATE = {
  open: true,
  splitLeft: 33,
  splitBottom: 50,
  fullSize: isMobile(),
  builtins: true,
};

export const ReconcileExample: LC = () => {
  const fiber = useFiber();

  return (
    <UseInspect
      fiber={fiber}
      appearance={APPEARANCE}
      initialState={INSPECT_STATE}
      save={false}
      findFiber={3}
    >
      <Reconcile>
        <Layout>
          <Quote>
            <Div>
              <Div>
                <Unquote>
                  <Header>
                    <Quote>
                      <Span>
                        <Unquote>
                          <Logo>
                            <Quote>
                              <Img />
                            </Quote>
                          </Logo>
                        </Unquote>
                      </Span>
                    </Quote>
                  </Header>
                </Unquote>
              </Div>
            </Div>
          </Quote>
        </Layout>
      </Reconcile>
    </UseInspect>
  );
};

const Layout = (props) => props.children;
const Header = (props) => props.children;
const Logo = (props) => props.children;

const Div = (props) => props.children;
const Span = (props) => props.children;
const Img = (props) => props.children;

// Bundler insists on renaming these
Layout.displayName = 'Layout';
Header.displayName = 'Header';

Div.displayName = 'Div';
Span.displayName = 'Span';
Img.displayName = 'Img';
